# eHR免费人力资源管理系统

#### 介绍
eHR免费人力资源管理系统,人力资源信息化,企业信息化,企业智能化

#### 软件架构
软件架构说明,B/S程序，部署简单，客户端使用浏览器进行访问
客户端无需安装插件，系统运行效率高效，架构采用国际先进的软件架构


#### 安装教程

1.  下载eHR.exe,setting.ini
2.  下载数据库文件
3.  服务器安装MS SQL，并附加数据库文件
4.  修改程序配置文件setting.ini
5.  运行eHR.exe
6.  通过浏览器访问服务器的ip地址

#### 使用说明

演示地址:http://ehr.kopsoft.cn/

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
